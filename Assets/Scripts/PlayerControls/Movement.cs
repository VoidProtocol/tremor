﻿using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class Movement : MonoBehaviour
{
    [Header("Settings:")]
    [SerializeField] private float _playerSpeed;

    private CharacterController _controller;

    private void Awake()
    {
        _controller = GetComponent<CharacterController>();
    }

    private void Update()
    {
        //Setting vector3 so we move based on direction we're facing
        Vector3 movementVector = GameInput.MovementValues.x * transform.right + GameInput.MovementValues.z * transform.forward;
        _controller.Move(movementVector * _playerSpeed * Time.deltaTime);
    }
}
