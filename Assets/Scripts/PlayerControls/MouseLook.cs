﻿using UnityEngine;

public class MouseLook : MonoBehaviour
{
    [Header("Setup:")]
    [SerializeField] private Transform _player;

    [Header("Settings:")]
    [SerializeField] private float _mouseSensitivity;

    private float _xRotation;

    private void Update()
    {
        RotateXAxis();
        RotateYAxis();
    }

    //Rotate camera and clamp values based on mouse input
    private void RotateXAxis()
    {
        _xRotation -= GameInput.LookDirection.y * _mouseSensitivity * Time.deltaTime;
        _xRotation = Mathf.Clamp(_xRotation, -90.0f, 90.0f);

        transform.localRotation = Quaternion.Euler(_xRotation, 0.0f, 0.0f);
    }

    //Rotate player based on mouse input
    private void RotateYAxis()
    {
        _player.Rotate(Vector3.up * GameInput.LookDirection.x * _mouseSensitivity * Time.deltaTime);
    }
}
