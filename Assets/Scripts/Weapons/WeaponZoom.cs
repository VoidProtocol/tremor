﻿using UnityEngine;

public class WeaponZoom : MonoBehaviour
{
    [Header("Setup:")]
    [SerializeField] private Camera _playerCamera;

    [Header("Settings:")]
    [SerializeField] private float _zoomAmount;
    [SerializeField] private float _zoomSpeed;

    private float _defaultFOV;
    private float _zoomFOV;

    private void Awake()
    {
        _defaultFOV = _playerCamera.fieldOfView;
        _zoomFOV = _defaultFOV / _zoomAmount;
    }

    private void Update()
    {
        if (GameInput.ZoomHold && _playerCamera.fieldOfView != _zoomFOV)
        {
            ZoomIn();
        }
        if (!GameInput.ZoomHold && _playerCamera.fieldOfView != _defaultFOV)
        {
            ZoomOut();
        }
    }

    private void ZoomIn()
    {
        _playerCamera.fieldOfView -= Time.deltaTime * _zoomSpeed;

        if (_playerCamera.fieldOfView < _zoomFOV)
        {
            _playerCamera.fieldOfView = _zoomFOV;
        }
    }

    private void ZoomOut()
    {
        _playerCamera.fieldOfView += Time.deltaTime * _zoomSpeed;

        if (_playerCamera.fieldOfView > _defaultFOV)
        {
            _playerCamera.fieldOfView = _defaultFOV;
        }
    }
}
