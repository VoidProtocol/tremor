﻿using UnityEngine;

public class WeaponAttack : MonoBehaviour
{
    [Header("Setup:")]
    [SerializeField] private Weapon _weapon;

    [Header("Weapon Config:")]
    [SerializeField] private float _delayBetweenAttacking;
    [SerializeField] private float _ammo;
    [SerializeField] private bool _infiniteAmmoMode;

    private float _nextTimeToAttack = 0.0f;

    private void Update()
    {
        if (GameInput.FireButtonPressed)
        {
            CheckIfAbleToAttack();
        }
    }

    //We check if weapon attack delay has passed and if we have ammo or infinite ammo mode enabled, then substract ammo and call
    //function for respective weapon type
    private void CheckIfAbleToAttack()
    {
        if (Time.time >= _nextTimeToAttack && (_ammo > 0 || _infiniteAmmoMode))
        {
            _nextTimeToAttack = Time.time + _delayBetweenAttacking;

            if (!_infiniteAmmoMode)
            {
                _ammo--;
            }

            _weapon.Attack();
        }
    }
}
