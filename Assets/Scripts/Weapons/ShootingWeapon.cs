﻿using System;
using UnityEngine;

[RequireComponent(typeof(WeaponAttack))]
public class ShootingWeapon : Weapon
{
    [Header("Setup:")]
    [SerializeField] private Camera _playerCamera;
    [SerializeField] private Animator _recoilAnimation;
    [SerializeField] private ParticleSystem[] _particles;

    [Header("Settings:")]
    [SerializeField] private TargetTag _targetTag = 0;

    private string _targetTagForCurrentWeapon;

    private void Start()
    {
        switch (_targetTag)
        {
            case TargetTag.smallGunTarget:
                _targetTagForCurrentWeapon = ConstLibrary.SGtarget;
                break;
            case TargetTag.machineGunTarget:
                _targetTagForCurrentWeapon = ConstLibrary.MGtarget;
                break;
            default:
                Debug.LogError("Selected tag index out of range.");
                break;
        }
    }

    public override void Attack()
    {
        PlayWeaponAnimations();

        RaycastHit hit;
        if (Physics.Raycast(_playerCamera.transform.position, _playerCamera.transform.forward, out hit))
        {
            if (hit.collider.CompareTag(_targetTagForCurrentWeapon))
            {
                hit.collider.GetComponent<Target>().TargetHit();
                Destroy(hit.collider.gameObject);
            }
        }
    }
    private void PlayWeaponAnimations()
    {
        _recoilAnimation.SetTrigger(ConstLibrary.Recoil);

        foreach (ParticleSystem particle in _particles)
        {
            particle.Play();
        }
    }
}

public enum TargetTag
{
    smallGunTarget = 0,
    machineGunTarget = 1
}
