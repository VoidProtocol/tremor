﻿using UnityEngine;

public class WeaponSwitch : MonoBehaviour
{
    [Header("Setup:")]
    [SerializeField] private GameObject _smallGun;
    [SerializeField] private GameObject _machineGun;
    [SerializeField] private GameObject _grenade;
    [Space]
    [SerializeField] private GameObject _smallGunCrosshair;
    [SerializeField] private GameObject _machineGunCrosshair;
    [SerializeField] private GameObject _grenadeCrosshair;

    [Header("Settings:")]
    [Tooltip("Set weapon to start with")]
    [SerializeField] private WeaponId _weaponId = 0;

    private int _numberOfWeapons = System.Enum.GetNames(typeof(WeaponId)).Length;
    private GameObject _currentWeapon;
    private GameObject _currentCrosshair;

    private void Awake()
    {
        SelectWeapon();
    }

    private void Update()
    {
        if (GameInput.SwitchWeaponPressed)
        {
            SwitchWeapon();
        }
    }

    
    private void SwitchWeapon()
    {
        _weaponId++;

        if ((int)_weaponId >= _numberOfWeapons)
        {
            _weaponId = 0;
        }

        SelectWeapon();
    }

    //Activate weapon and crosshair based on current weapon ID
    private void SelectWeapon()
    {
        _currentWeapon?.SetActive(false);
        _currentCrosshair?.SetActive(false);

        switch (_weaponId)
        {
            case WeaponId.smallGun:
                _smallGun.SetActive(true);
                _smallGunCrosshair.SetActive(true);
                _currentWeapon = _smallGun;
                _currentCrosshair = _smallGunCrosshair;
                break;
            case WeaponId.machineGun:
                _machineGun.SetActive(true);
                _machineGunCrosshair.SetActive(true);
                _currentWeapon = _machineGun;
                _currentCrosshair = _machineGunCrosshair;
                break;
            case WeaponId.grenade:
                _grenade.SetActive(true);
                _grenadeCrosshair.SetActive(true);
                _currentWeapon = _grenade;
                _currentCrosshair = _grenadeCrosshair;
                break;
            default:
                Debug.LogError("Selected weapon index out of range.");
                break;
        }
    }
}

public enum WeaponId
{
    smallGun = 0,
    machineGun = 1,
    grenade = 2,
}

