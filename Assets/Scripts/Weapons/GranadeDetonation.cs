﻿using UnityEngine;

public class GranadeDetonation : MonoBehaviour
{
    [Header("Setup:")]
    [SerializeField] private Collider _granadeExplosionCollider;
    [SerializeField] private ParticleSystem _explosionParticle;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag(ConstLibrary.Floor))
        {
            _granadeExplosionCollider.enabled = true;
            _explosionParticle.Play();
            Destroy(gameObject, 0.2f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag(ConstLibrary.SGtarget) || other.gameObject.CompareTag(ConstLibrary.MGtarget))
        {
            other.gameObject.GetComponent<Target>().TargetHit();
            Destroy(other.gameObject);
        }
    }
}
