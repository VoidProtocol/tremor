﻿using UnityEngine;

[RequireComponent(typeof(WeaponAttack))]
public class ThrowableWeapon : Weapon
{
    [Header("Setup:")]
    [SerializeField] private Camera _playerCamera;
    [SerializeField] private GameObject _throwablePrefab;

    [Header("Settings:")]
    [SerializeField] private float _throwForce;

    public override void Attack()
    {
        GameObject throwable = Instantiate(_throwablePrefab, _playerCamera.transform.position, _playerCamera.transform.rotation);
        throwable.GetComponent<Rigidbody>().AddForce(_playerCamera.transform.forward * _throwForce, ForceMode.Impulse);
    }
}
