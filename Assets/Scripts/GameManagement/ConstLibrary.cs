﻿using UnityEngine;

public class ConstLibrary : MonoBehaviour
{
    //Tags
    public const string Floor = "Floor";
    public const string SGtarget = "SGtarget";
    public const string MGtarget = "MGtarget";
    
    //Functions
    public const string Restart = "Restart";

    //Animation Triggers
    public const string Recoil = "Recoil";
}
