﻿using UnityEngine;

public class CursorLock : MonoBehaviour
{
    [Header("Settings:")]
    [SerializeField] private bool _cursorLockEnabled;

    private void Awake()
    {
        if (_cursorLockEnabled)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}
