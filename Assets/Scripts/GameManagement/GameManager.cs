﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("Setup")]
    [SerializeField] private GameObject _targetsGameObj;
    [SerializeField] private GameObject _gameOverScreen;

    [Header("Settings")]
    [SerializeField] private float _timeToRestart;

    private static int _numberOfTargets;

    private void Start()
    {
        foreach (Target target in _targetsGameObj.GetComponentsInChildren<Target>())
        {
            _numberOfTargets++;
            target.OnTargetHit += HandleDestroyedTarget;
        }
    }

    public void HandleDestroyedTarget(Target target)
    {
        _numberOfTargets--;
        target.OnTargetHit -= HandleDestroyedTarget;

        if (_numberOfTargets <= 0)
        {
            _gameOverScreen.SetActive(true);
            Invoke(ConstLibrary.Restart, _timeToRestart);
        }
    }

    private void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
