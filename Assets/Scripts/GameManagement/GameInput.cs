﻿using UnityEngine;

public class GameInput : MonoBehaviour
{
    public static Vector3 MovementValues { get; private set; }
    public static Vector2 LookDirection { get; private set; }
    public static bool FireButtonPressed { get; private set; }
    public static bool SwitchWeaponPressed { get; private set; }
    public static bool ZoomHold { get; private set; }

    void Update()
    {
        //Getting input for other classes
        MovementValues = new Vector3(Input.GetAxisRaw("Horizontal"), 0.0f, Input.GetAxisRaw("Vertical")).normalized;
        LookDirection = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        FireButtonPressed = Input.GetButton("Fire1");
        SwitchWeaponPressed = Input.GetKeyDown("q");
        ZoomHold = Input.GetButton("Fire2");
    }
}
