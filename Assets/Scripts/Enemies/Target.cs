﻿using System;
using UnityEngine;

public class Target : MonoBehaviour
{
    public event Action<Target> OnTargetHit;

    public void TargetHit()
    {
        OnTargetHit?.Invoke(this);
    }
}
